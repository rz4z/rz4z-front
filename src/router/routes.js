const routes = [
  {
    path: '/',
    component: () => import('layouts/Layout.vue'),
    children: [{
      name: 'home',
      path: '/',
      component: () => import('pages/Index.vue')
    }, {
      name: 'test',
      path: 'test',
      component: () => import('components/studying/TestComponent.vue')
    }]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
