FROM node:8-alpine
MAINTAINER Dmitrii Dmitriev useroid@gmail.com

WORKDIR /opt/rz4z-front

# Install system dependencies
RUN set -x \
    && apk update \
    && apk add --no-cache \
        build-base \
        musl \
        pngquant \
        zlib-dev \
    && npm config set unsafe-perm true \
    && npm install --global \
        @quasar/cli

# Install project packages
COPY package.json package-lock.json ./
RUN set -x \
    && npm install

ARG SENTRY_DSN
ENV SENTRY_DSN=$SENTRY_DSN

COPY ./ ./

# Build project
RUN set -x \
    && quasar build

# Just quit
CMD ["true"]
