import * as Sentry from '@sentry/browser'
import * as Integrations from '@sentry/integrations'

export default async ({ Vue }) => {
  if (process.env.SENTRY_DSN) {
    Sentry.init({
      dsn: process.env.SENTRY_DSN,
      integrations: [new Integrations.Vue({ Vue, attachProps: true })]
    })
  }
}
